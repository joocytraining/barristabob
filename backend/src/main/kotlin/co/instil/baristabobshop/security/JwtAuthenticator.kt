/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.security

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import java.util.Date
import java.util.concurrent.TimeUnit

class JwtAuthenticator {

    private val algorithm = Algorithm.HMAC512("SECRET")
    private val jwtVerifier = JWT.require(algorithm).build()

    fun isTokenValid(token: String) = try { jwtVerifier.verify(token) != null } catch (e: Exception) { false }

    fun generateToken(username: String): String {
        return JWT.create()
                .withSubject(username)
                .withExpiresAt(Date(System.currentTimeMillis() + TimeUnit.DAYS.toMillis(30)))
                .sign(algorithm)
                .orEmpty()
    }
}