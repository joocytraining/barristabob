package co.instil.baristabobshop.persistence

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drink.persistence.DrinkDatabase
import co.instil.baristabobshop.extensions.blockingObserve
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.hasItem
import org.hamcrest.MatcherAssert.assertThat
import org.junit.After
import org.junit.Test
import org.junit.runner.RunWith
import java.util.Date

@RunWith(AndroidJUnit4::class)
class DrinkDatabaseTest {

    private val target =
        Room.inMemoryDatabaseBuilder(InstrumentationRegistry.getContext(), DrinkDatabase::class.java)
            .allowMainThreadQueries()
            .build()

    private val drink1 = createDrink(name = "First Drink", timeStamp = Date(100))
    private val drink2 = createDrink(name = "Second Drink", timeStamp = Date(100))

    @After
    fun cleanUp() {
        target.close()
    }

    @Test
    fun shouldAddListOfDrinksToDatabase() {
        saveDrinks(listOf(drink1, drink2))

        val drinkNames = target.drinkDatastore().getAllDrinks().blockingObserve()?.map(Drink::name)

        assertThat(drinkNames, hasItem(drink1.name))
        assertThat(drinkNames, hasItem(drink2.name))
    }

    @Test
    fun shouldReturnTheCorrectNumberOfDrinksOnCount() {
        saveDrinks(listOf(drink1, drink1, drink1, drink1))
        val numberOfDrinksInDatabase = getAllDrinks().blockingObserve()?.size

        val countOfDrinks = getDrinkCount()

        assertThat(countOfDrinks, `is`(numberOfDrinksInDatabase))
    }

    @Test
    fun shouldReturnTheDrinkWithTheLatestTimestampOnGetLatestDrink() {
        val testDrinkLate = createDrink(name = "Late Drink", timeStamp = Date(1000000))
        val listOfDrinks = listOf(drink1, testDrinkLate, drink2, drink1)
        saveDrinks(listOfDrinks)

        val latestDrink = getLatestDrink()

        assertThat(latestDrink.name, `is`(testDrinkLate.name))
    }

    private fun getLatestDrink() = target.drinkDatastore().getLatestDrink()

    private fun saveDrinks(listOfDrinks: List<Drink>) = target.drinkDatastore().addListOfDrinks(listOfDrinks)

    private fun getAllDrinks() = target.drinkDatastore().getAllDrinks()

    private fun getDrinkCount() = target.drinkDatastore().getDrinkCount()
}