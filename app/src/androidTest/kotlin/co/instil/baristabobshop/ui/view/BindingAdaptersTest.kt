package co.instil.baristabobshop.ui.view

import android.graphics.drawable.Drawable
import android.support.test.InstrumentationRegistry
import android.widget.ImageView
import co.instil.baristabobshop.R
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class BindingAdaptersTest {

    private val context = InstrumentationRegistry.getContext()

    private val imageView = ImageView(context)
    private val imageLocation = R.drawable.ic_coffee_cup

    @Test
    fun shouldChangeImageViewDrawable() {
        setImageViewResource(imageView, imageLocation)
        val imageDrawable = Drawable.createFromPath("R.drawable.ic_coffee_cup")

        assertThat(imageView.drawable, `is`(imageDrawable))
    }
}