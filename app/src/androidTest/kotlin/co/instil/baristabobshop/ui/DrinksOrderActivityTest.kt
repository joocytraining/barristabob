/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.ui

import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.dagger.DaggerApplicationInitializer
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.activity.DrinksOrderActivity
import co.instil.baristabobshop.drinksorder.service.DrinksOrderService
import co.instil.baristabobshop.espresso.assertThatViewIsDisplayed
import co.instil.baristabobshop.espresso.assertThatViewIsEnabled
import co.instil.baristabobshop.espresso.assertThatViewIsNotDisplayed
import co.instil.baristabobshop.espresso.assertThatViewIsNotEnabled
import co.instil.baristabobshop.espresso.clickView
import co.instil.baristabobshop.navigation.NavigationRouter
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class DrinksOrderActivityTest {

    @Rule @JvmField val activityTestRule = ActivityTestRule<DrinksOrderActivity>(DrinksOrderActivity::class.java, true, false)
    @Rule @JvmField val daggerApplicationRule = DaggerApplicationInitializer(this)

    @Inject lateinit var drinksOrderService: DrinksOrderService
    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var navigationRouter: NavigationRouter

    private val orderList = listOf(
        DrinkOrderDetails(1, 1, createDrink()),
        DrinkOrderDetails(2, 2, createDrink()),
        DrinkOrderDetails(3, 3, createDrink())
    )

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        given(resourceService.getStringResource(R.string.format_two_decimal_place)).willReturn("%.2f")
        given(resourceService.getStringResource(R.string.gbp_symbol)).willReturn("£")
        given(resourceService.getStringResource(R.string.prefix_quantity)).willReturn("Quantity: ")
    }

    @Test
    fun shouldDisplayBaristaBobLogo() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.orderCoffeeIconImageView)
    }

    @Test
    fun shouldDisplayTitle() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.orderAppTitleTextView)
    }

    @Test
    fun shouldDisplayEmptyListTextWhenNoDrinksOrdersAreReturned() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.drinksOrderEmptyStateText)
    }

    @Test
    fun shouldNotDisplayRecyclerViewWhenNoDrinksAreReturned() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsNotDisplayed(R.id.drinksOrderRecyclerView)
    }

    @Test
    fun shouldDisplayRecyclerViewWhenDrinksAreReturned() {
        setDrinksOrderServiceToReturnTestDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.drinksOrderRecyclerView)
    }

    @Test
    fun shouldDisplayEnabledAddToOrderButton() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.addToOrderButton)
        assertThatViewIsEnabled(R.id.addToOrderButton)
    }

    @Test
    fun shouldHaveHiddenDisabledCheckoutButtonWhenListIsEmpty() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        assertThatViewIsNotDisplayed(R.id.checkoutButton)
        assertThatViewIsNotEnabled(R.id.checkoutButton)
    }

    @Test
    fun shouldDisplayEnabledCheckoutButtonWhenListIsNotEmpty() {
        setDrinksOrderServiceToReturnTestDrinks()
        launchActivity()

        assertThatViewIsDisplayed(R.id.checkoutButton)
        assertThatViewIsEnabled(R.id.checkoutButton)
    }

    @Test
    fun shouldNavigateToDrinksMenuOnAddToOrderButtonClicked() {
        setDrinksOrderServiceToReturnNoDrinks()
        launchActivity()

        clickView(R.id.addToOrderButton)

        verify(navigationRouter).navigateToDrinksMenu()
    }

    private fun launchActivity() {
        activityTestRule.launchActivity(null)
    }

    private fun setDrinksOrderServiceToReturnNoDrinks() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(emptyList()))
    }

    private fun setDrinksOrderServiceToReturnTestDrinks() {
        given(drinksOrderService.getAllDrinksOrderItems()).willReturn(Single.just(orderList))
    }
}