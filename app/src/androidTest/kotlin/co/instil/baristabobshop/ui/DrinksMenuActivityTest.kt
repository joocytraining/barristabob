package co.instil.baristabobshop.ui

import android.arch.lifecycle.LiveData
import android.support.test.rule.ActivityTestRule
import android.support.test.runner.AndroidJUnit4
import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.dagger.DaggerApplicationInitializer
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drinksmenu.activity.DrinksMenuActivity
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.espresso.assertThatActionBarTitleIsSetTo
import co.instil.baristabobshop.espresso.assertThatRecyclerViewHasChildCount
import co.instil.baristabobshop.espresso.assertThatViewIsDisplayed
import co.instil.baristabobshop.espresso.assertThatViewIsNotDisplayed
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import io.reactivex.Completable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
class DrinksMenuActivityTest {

    @Rule @JvmField val activityTestRule = ActivityTestRule<DrinksMenuActivity>(DrinksMenuActivity::class.java, true, false)
    @Rule @JvmField val daggerApplicationRule = DaggerApplicationInitializer(this)

    @Inject lateinit var resourceService: ResourceService
    @Inject lateinit var drinksMenuService: DrinksMenuService

    private val listOfDrinks = listOf(
        createDrink(),
        createDrink(),
        createDrink()
    )

    private val liveData = mock<LiveData<List<Drink>>>()

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        setDrinksServerUpdateToCompleteInstantly()
        setDrinksMenuServiceToReturnDrinksList()
    }

    @Test
    fun shouldShowLoadingSpinnerAndNotRecyclerViewWhileLoadingDrinks() {
        setDrinksMenuServiceToNeverReturn()
        launchActivity()

        assertThatViewIsDisplayed(R.id.drinksMenuLoadingSpinner)
        assertThatViewIsNotDisplayed(R.id.drinksMenuRecyclerView)
    }

    @Test
    fun shouldShowRecyclerViewAndNotLoadingSpinnerAfterDrinksAreLoaded() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.drinksMenuRecyclerView)
        assertThatViewIsNotDisplayed(R.id.drinksMenuLoadingSpinner)
    }

    @Test
    fun shouldHaveAnEmptyRecyclerViewBeforeDataIsUpdated() {
        launchActivity()

        assertThatRecyclerViewHasChildCount(R.id.drinksMenuRecyclerView, 0)
    }

    @Test
    fun shouldDisplayActionBar() {
        launchActivity()

        assertThatViewIsDisplayed(R.id.action_bar)
    }

    @Test
    fun shouldHaveActionBarTitleSet() {
        launchActivity()

        assertThatActionBarTitleIsSetTo(activityTestRule.activity.supportActionBar!!, "Drinks Menu")
    }

    private fun launchActivity() {
        activityTestRule.launchActivity(null)
    }

    private fun setDrinksMenuServiceToNeverReturn() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.never())
    }

    private fun setDrinksMenuServiceToReturnDrinksList() {
        given(drinksMenuService.getAllDrinks()).willReturn(liveData)
        given(liveData.value).willReturn(listOfDrinks)
    }

    private fun setDrinksServerUpdateToCompleteInstantly() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.complete())
    }
}