package co.instil.baristabobshop.api

import okhttp3.mockwebserver.MockResponse
import org.junit.Rule

open class ApiTest {

    @Rule @JvmField val apiServerMock = ApiServerMock()

    fun enqueueResponse(response: MockResponse) = apiServerMock.server.enqueue(response)
}