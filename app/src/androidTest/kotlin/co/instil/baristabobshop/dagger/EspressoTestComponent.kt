package co.instil.baristabobshop.dagger

import co.instil.baristabobshop.ui.CheckoutActivityTest
import co.instil.baristabobshop.ui.DrinksMenuActivityTest
import co.instil.baristabobshop.ui.DrinksOrderActivityTest
import co.instil.baristabobshop.ui.LoginActivityTest
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [TestAppModule::class])
interface EspressoTestComponent : ApplicationComponent {

    fun inject(loginActivityTest: LoginActivityTest)
    fun inject(drinksMenuActivityTest: DrinksMenuActivityTest)
    fun inject(checkoutActivityTest: CheckoutActivityTest)
    fun inject(drinksOrderActivityTest: DrinksOrderActivityTest)
}