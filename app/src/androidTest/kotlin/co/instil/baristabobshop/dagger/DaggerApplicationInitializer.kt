package co.instil.baristabobshop.dagger

import android.support.test.InstrumentationRegistry
import co.instil.baristabobshop.ui.CheckoutActivityTest
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.ui.DrinksMenuActivityTest
import co.instil.baristabobshop.ui.DrinksOrderActivityTest
import co.instil.baristabobshop.ui.LoginActivityTest
import org.junit.rules.ExternalResource

class DaggerApplicationInitializer(private val testClass: Any) : ExternalResource() {

    override fun before() {
        val application = InstrumentationRegistry
                .getInstrumentation()
                .targetContext
                .applicationContext as Application

        val component = DaggerEspressoTestComponent
                .builder()
                .testAppModule(TestAppModule(application))
                .build()

        Application.dependencies = component

        inject(testClass, component)
    }

    fun inject(testClass: Any, component: EspressoTestComponent) {
        when (testClass) {
            // activities
            is LoginActivityTest -> component.inject(testClass)
            is DrinksMenuActivityTest -> component.inject(testClass)
            is CheckoutActivityTest -> component.inject(testClass)
            is DrinksOrderActivityTest -> component.inject(testClass)
        }
    }
}