package co.instil.baristabobshop.dagger

import android.arch.persistence.room.Room
import android.content.Context
import androidx.work.WorkManager
import co.instil.baristabobshop.checkout.service.CheckoutService
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.drink.persistence.DrinkDatabase
import co.instil.baristabobshop.drink.persistence.DrinkDatastore
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.drinksorder.service.DrinksOrderService
import co.instil.baristabobshop.login.service.LoginService
import co.instil.baristabobshop.navigation.NavigationRouter
import co.instil.baristabobshop.prompts.ToastPrompt
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.scheduler.ImmediateBaristaBobAndroidTestSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.mock
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class TestAppModule(private val application: Application) {

    @Singleton
    @Provides
    fun provideApplication(): android.app.Application {
        return application
    }

    @Singleton
    @Provides
    fun provideApplicationContext(): Context {
        return application
    }

    @Singleton
    @Provides
    fun provideNavigationRouter(): NavigationRouter = mock()

    @Singleton
    @Provides
    fun provideLogInService(): LoginService = mock()

    @Singleton
    @Provides
    fun provideCheckoutService(): CheckoutService = mock()

    @Singleton
    @Provides
    fun provideDrinksMenuService(): DrinksMenuService = mock()

    @Singleton
    @Provides
    fun provideDrinksOrderService(): DrinksOrderService = mock()

    @Singleton
    @Provides
    fun provideBaristaBobSchedulers(): BaristaBobSchedulers = ImmediateBaristaBobAndroidTestSchedulers()

    @Singleton
    @Provides
    fun provideResourceService(): ResourceService = mock()

    @Provides
    @Singleton
    fun provideDrinkDatabase(context: Context): DrinkDatabase = Room.databaseBuilder(context, DrinkDatabase::class.java, "Drink_Database").fallbackToDestructiveMigration().build()

    @Provides
    fun provideDrinkDatastore(drinkDatabase: DrinkDatabase): DrinkDatastore = drinkDatabase.drinkDatastore()

    @Provides
    fun provideWorkManager(): WorkManager = mock()

    @Provides
    @Singleton
    fun provideToastPrompt(): ToastPrompt = mock()
}