package co.instil.baristabobshop.espresso

import android.support.annotation.IdRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.action.ViewActions.click
import android.support.test.espresso.action.ViewActions.scrollTo
import android.support.test.espresso.action.ViewActions.typeText
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.withId

fun scrollToThenTypeTextIntoView(@IdRes viewId: Int, text: String) {
    onView(withId(viewId)).perform(scrollTo(), typeText(text))
}

fun scrollToThenClickView(@IdRes viewId: Int) {
    onView(withId(viewId)).perform(scrollTo(), click())
}

fun clickView(@IdRes viewId: Int) {
    onView(withId(viewId)).perform(click())
}

fun typeTextIntoView(@IdRes viewId: Int, text: String) {
    onView(withId(viewId))
        .check(matches(isDisplayed()))
        .perform(typeText(text))
}

fun scrollToViewWithId(@IdRes id: Int) {
    onView(withId(id))
        .perform(scrollTo())
}
