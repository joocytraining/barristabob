package co.instil.baristabobshop.espresso

import android.support.annotation.IdRes
import android.support.test.espresso.Espresso.onView
import android.support.test.espresso.assertion.ViewAssertions.matches
import android.support.test.espresso.matcher.RootMatchers
import android.support.test.espresso.matcher.ViewMatchers.hasChildCount
import android.support.test.espresso.matcher.ViewMatchers.isDisplayed
import android.support.test.espresso.matcher.ViewMatchers.isEnabled
import android.support.test.espresso.matcher.ViewMatchers.isNotChecked
import android.support.test.espresso.matcher.ViewMatchers.withId
import android.support.test.espresso.matcher.ViewMatchers.withText
import android.support.v7.app.ActionBar
import co.instil.baristabobshop.extensions.untilAssertedOrTimeout
import org.awaitility.Awaitility.await
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.hamcrest.MatcherAssert.assertThat

fun assertThatViewIsDisplayed(@IdRes viewId: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(isDisplayed()))
    }
}

fun assertThatViewIsNotDisplayed(@IdRes viewId: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(not(isDisplayed())))
    }
}

fun assertThatViewIsEnabled(@IdRes viewId: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(isEnabled()))
    }
}

fun assertThatViewIsNotEnabled(@IdRes viewId: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(not(isEnabled())))
    }
}

fun assertThatRecyclerViewHasChildCount(@IdRes viewId: Int, childCount: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(hasChildCount(childCount)))
    }
}

fun assertThatActionBarTitleIsSetTo(actionBar: ActionBar, titleString: String) {
    await().untilAssertedOrTimeout {
        assertThat(actionBar.title.toString(), `is`(titleString))
    }
}

fun assertThatDialogWithTextIsDisplayed(expectedText: String) {
    await().untilAssertedOrTimeout {
        onView(withText(expectedText))
            .inRoot(RootMatchers.isDialog())
            .check(matches(isDisplayed()))
    }
}

fun assertThatRadioButtonIsNotChecked(@IdRes viewId: Int) {
    await().untilAssertedOrTimeout {
        onView(withId(viewId))
            .check(matches(isNotChecked()))
    }
}