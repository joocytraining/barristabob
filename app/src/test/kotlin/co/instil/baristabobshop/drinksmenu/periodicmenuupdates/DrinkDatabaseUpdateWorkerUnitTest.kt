/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksmenu.periodicmenuupdates

import androidx.work.Worker
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class DrinkDatabaseUpdateWorkerUnitTest {

    private val drinksMenuService = mock<DrinksMenuService>()

    private val target = DrinksDatabaseUpdateWorker()

    @Before
    fun beforeEachTest() {
        target.drinksMenuService = drinksMenuService
    }

    @Test
    fun shouldCallUpdateDrinksMenuWhenDoWorkIsCalled() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.complete())

        target.doWork()

        verify(drinksMenuService).updateDrinksMenu()
    }

    @Test
    fun shouldReturnSuccessWhenUpdateDrinksMenuCompletes() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.complete())

        assertThat(target.doWork(), `is`(Worker.Result.SUCCESS))
    }

    @Test
    fun shouldReturnFailureWhenUpdateDrinksMenuThrowsAnError() {
        given(drinksMenuService.updateDrinksMenu()).willReturn(Completable.error(Error()))

        assertThat(target.doWork(), `is`(Worker.Result.FAILURE))
    }
}