package co.instil.baristabobshop.login

import co.instil.baristabobshop.R
import co.instil.baristabobshop.login.service.LoginService
import co.instil.baristabobshop.login.viewmodel.LoginViewModel
import co.instil.baristabobshop.navigation.NavigationRouter
import co.instil.baristabobshop.scheduler.ImmediateBaristaBobTestSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test
import retrofit2.HttpException
import retrofit2.Response

class LoginViewModelUnitTest {

    private val resourceService = mock<ResourceService>()
    private val loginService = mock<LoginService>()
    private val navigationRouter = mock<NavigationRouter>()

    private val username = "instil"
    private val password = "password"

    private val target = LoginViewModel(loginService, navigationRouter)

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.login_error_dialog_title)).willReturn("Login Unsuccessful")
        given(resourceService.getStringResource(R.string.login_unknown_error_dialog_message)).willReturn("Your Login has failed in an unexpected way :(")
        given(resourceService.getStringResource(R.string.login_error_dialog_message_http_400)).willReturn("Login failed due to 400 error")

        target.resourceService = resourceService
        target.baristaBobSchedulers = ImmediateBaristaBobTestSchedulers()
        target.dialogFactory = mock()
        target.username.set(username)
        target.password.set(password)
    }

    @Test
    fun shouldEnableLogInButtonWhenUsernameAndPasswordSet() {
        target.onLoginInformationChanged()

        assertThat(target.loginButtonsEnabled.get(), `is`(true))
    }

    @Test
    fun shouldDisableLogInButtonWhenPasswordIsEmpty() {
        target.onLoginInformationChanged()

        target.password.set("")
        target.onLoginInformationChanged()

        assertThat(target.loginButtonsEnabled.get(), `is`(false))
    }

    @Test
    fun shouldDisableLogInButtonWhenUsernameIsEmpty() {
        target.onLoginInformationChanged()

        target.username.set("")
        target.onLoginInformationChanged()

        assertThat(target.loginButtonsEnabled.get(), `is`(false))
    }

    @Test
    fun shouldNavigateToDrinksOrderWhenLoggedInSuccessfully() {
        given(loginService.login(username, password)).willReturn(Completable.complete())

        target.onLoginButtonClicked()

        verify(navigationRouter).navigateToDrinksOrder()
    }

    @Test
    fun shouldDisplayUnknownErrorResponseDialogWhen500ErrorIsReturnedFromLoginService() {
        givenLoginFails(HttpException(Response.error<String>(506, mock())))

        target.onLoginButtonClicked()

        verify(target.dialogFactory)?.confirmationDialog(resourceService.getStringResource(R.string.login_error_dialog_title), resourceService.getStringResource(R.string.login_unknown_error_dialog_message))
    }

    @Test
    fun shouldDisplayHttp400ErrorResponseDialogWhen400ErrorIsReturnedFromLogin() {
        givenLoginFails(HttpException(Response.error<String>(405, mock())))

        target.onLoginButtonClicked()

        verify(target.dialogFactory)?.confirmationDialog(resourceService.getStringResource(R.string.login_error_dialog_title), resourceService.getStringResource(R.string.login_error_dialog_message_http_400))
    }

    @Test
    fun shouldDisplayUnknownErrorResponseDialogWhenAnyOtherHttpErrorIsReturnedFromLogin() {
        givenLoginFails(HttpException(Response.error<String>(600, mock())))

        target.onLoginButtonClicked()

        verify(target.dialogFactory)?.confirmationDialog(resourceService.getStringResource(R.string.login_error_dialog_title), resourceService.getStringResource(R.string.login_unknown_error_dialog_message))
    }

    @Test
    fun shouldDisplayUnknownErrorResponseDialogWhenNonHttpErrorIsReturnedFromLogin() {
        givenLoginFails()

        target.onLoginButtonClicked()

        verify(target.dialogFactory)?.confirmationDialog(resourceService.getStringResource(R.string.login_error_dialog_title), resourceService.getStringResource(R.string.login_unknown_error_dialog_message))
    }

    private fun givenLoginFails(error: Throwable = RuntimeException()) {
        given(loginService.login(username, password)).willReturn(Completable.error(error))
    }
}