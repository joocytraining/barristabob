package co.instil.baristabobshop.security

import co.instil.baristabobshop.security.exceptions.JwtNotInSharedPreferencesException
import co.instil.baristabobshop.services.Base64Service
import co.instil.baristabobshop.services.SharedPreferenceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

class JwtServiceUnitTest {

    private val encryptionService = mock<EncryptionService>()
    private val sharedPreferenceService = mock<SharedPreferenceService>()
    private val base64Service = mock<Base64Service>()

    private val jwt = "1389382982"
    private val encryptString = "Test String"
    private val encryptedJwt = "EnCyTpTeDJWT"

    private val target = JwtService(encryptionService, sharedPreferenceService, base64Service)

    @Test
    fun shouldRetrieveJwt() {
        givenSuccessfullyRetrieveJwt()

        val actualJwt = target.retrieveJwt()

        assertThat(actualJwt, `is`(jwt))
    }

    @Test(expected = JwtNotInSharedPreferencesException::class)
    fun shouldThrowExceptionWhenCannotRetrieveJwt() {
        target.retrieveJwt()
    }

    @Test(expected = JwtNotInSharedPreferencesException::class)
    fun shouldThrowExceptionWhenCannotRetrieveInitializationVector() {
        given(sharedPreferenceService.retrieveString("encrypted_jwt")).willReturn(encryptedJwt)

        target.retrieveJwt()
    }

    @Test
    fun shouldStoreJwt() {
        given(encryptionService.encrypt(jwt)).willReturn(EncryptionResult(ByteArray(0), ByteArray(0)))
        given(base64Service.encodeToString(ByteArray(0))).willReturn(encryptString)

        target.storeJwt(jwt)

        verifySuccessfullyStoreString()
    }

    private fun givenSuccessfullyRetrieveJwt() {
        val encryptedInitializationVector = "EnCyTpTeDIV"
        val jwtByteArray = byteArrayOf()
        val initializationVectorByteArray = byteArrayOf()

        given(sharedPreferenceService.retrieveString("encrypted_jwt")).willReturn(encryptedJwt)
        given(sharedPreferenceService.retrieveString("jwt_initialization_vector")).willReturn(encryptedInitializationVector)
        given(base64Service.decodeToByteArray(encryptedJwt)).willReturn(jwtByteArray)
        given(base64Service.decodeToByteArray(encryptedInitializationVector)).willReturn(initializationVectorByteArray)
        given(encryptionService.decrypt(jwtByteArray, initializationVectorByteArray)).willReturn(jwt)
    }

    private fun verifySuccessfullyStoreString() {
        verify(sharedPreferenceService).storeString("encrypted_jwt", encryptString)
        verify(sharedPreferenceService).storeString("jwt_initialization_vector", encryptString)
    }
}