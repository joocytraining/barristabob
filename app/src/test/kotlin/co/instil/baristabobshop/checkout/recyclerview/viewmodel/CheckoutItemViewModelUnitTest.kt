/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class CheckoutItemViewModelUnitTest {

    private val resourceService = mock<ResourceService>()

    private val drinksOrderItem = DrinkOrderDetails(4, 1, createDrink(name = "Coffee", cost = 3.00))

    private lateinit var target: CheckoutItemViewModel

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")
        given(resourceService.getStringResource(R.string.format_quantity)).willReturn("x %s")

        target = CheckoutItemViewModel(drinksOrderItem, resourceService)
    }

    @Test
    fun shouldReturnItemName() {
        assertThat(target.name, `is`("Coffee"))
    }

    @Test
    fun shouldFormatAndReturnCost() {
        assertThat(target.cost, `is`("£12.00"))
    }

    @Test
    fun shouldFormatAndReturnQuantity() {
        assertThat(target.quantity, `is`("x 4"))
    }
}