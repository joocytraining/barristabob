/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.builder.createDrink
import co.instil.baristabobshop.checkout.service.CheckoutService
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.navigation.NavigationRouter
import co.instil.baristabobshop.scheduler.ImmediateBaristaBobTestSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.given
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.never
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Completable
import io.reactivex.Single
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class CheckoutViewModelUnitTest {

    private val checkoutService = mock<CheckoutService>()
    private val navigationRouter = mock<NavigationRouter>()
    private val resourceService = mock<ResourceService>()

    private val drinkOrders = listOf(
        DrinkOrderDetails(2, 5, createDrink(cost = 2.00)),
        DrinkOrderDetails(6, 5, createDrink(cost = 1.00))
    )

    private val target = CheckoutViewModel(checkoutService, navigationRouter)

    @Before
    fun beforeEachTest() {
        given(resourceService.getStringResource(R.string.format_currency_gbp)).willReturn("£%.2f")

        target.resourceService = resourceService
        target.baristaBobSchedulers = ImmediateBaristaBobTestSchedulers()
    }

    @Test
    fun shouldNavigateToDrinksOrderOnCancelButtonClicked() {
        target.onCancelButtonClicked()

        verify(navigationRouter).navigateToDrinksOrder()
    }

    @Test
    fun shouldEnableConfirmButtonOnRadioButtonChanged() {
        target.onRadioButtonCheckedChanged()

        assertThat(target.confirmButtonEnabled.get(), `is`(true))
    }

    @Test
    fun shouldNavigateToDrinksOrderWhenDrinksListIsEmpty() {
        givenCheckoutServiceReturnsNoDrinks()

        target.getDrinksOrdersAndUpdateDetails().blockingGet()

        verify(navigationRouter).navigateToDrinksOrder()
    }

    @Test
    fun shouldCalculateTotalOfReturnedDrinksCorrectly() {
        givenCheckoutServiceReturnsValidDrinks()

        target.getDrinksOrdersAndUpdateDetails().blockingGet()

        assertThat(target.totalCost.get(), `is`("£10.00"))
    }

    @Test
    fun shouldDisableConfirmButtonWhenDataIsUpdated() {
        givenCheckoutServiceReturnsValidDrinks()
        target.onRadioButtonCheckedChanged()

        target.getDrinksOrdersAndUpdateDetails().blockingGet()

        assertThat(target.confirmButtonEnabled.get(), `is`(false))
    }

    @Test
    fun shouldNotCheckoutDrinksIfOrderIsNotSuccessful() {
        givenCheckoutServiceReturnsValidDrinks()
        givenSendOrderFails()

        target.getDrinksOrdersAndUpdateDetails()
        target.onConfirmButtonClicked()

        verify(checkoutService, never()).checkoutPendingDrinkOrders()
    }

    @Test
    fun shouldCheckoutDrinksIfOrderIsSuccessful() {
        givenCheckoutServiceReturnsValidDrinks()
        givenSendOrderIsSuccessful()

        target.getDrinksOrdersAndUpdateDetails()
        target.onConfirmButtonClicked()

        verify(checkoutService).checkoutPendingDrinkOrders()
    }
    @Test
    fun shouldNavigateToDrinksOrderWhenOrderIsSuccessfullyPlacedAndCheckedOut() {
        givenCheckoutServiceReturnsValidDrinks()
        givenSendOrderIsSuccessful()
        givenCheckoutPendingDrinkOrdersIsSuccessful()

        target.getDrinksOrdersAndUpdateDetails()
        target.cashButtonClicked.set(true)
        target.onConfirmButtonClicked()

        verify(navigationRouter).navigateToDrinksOrder()
    }

    @Test
    fun shouldNotNavigateIfCheckoutDrinksIsNotSuccessful() {
        givenCheckoutServiceReturnsValidDrinks()
        givenSendOrderIsSuccessful()
        givenCheckoutPendingDrinkOrdersFails()

        target.getDrinksOrdersAndUpdateDetails()
        target.onConfirmButtonClicked()

        verify(navigationRouter, never()).navigateToDrinksOrder()
    }

    private fun givenSendOrderIsSuccessful() {
        given(checkoutService.sendOrder(any(), any())).willReturn(Completable.complete())
    }

    private fun givenSendOrderFails() {
        given(checkoutService.sendOrder(any(), any())).willReturn(Completable.error(Error()))
    }

    private fun givenCheckoutPendingDrinkOrdersIsSuccessful() {
        given(checkoutService.checkoutPendingDrinkOrders()).willReturn(Completable.complete())
    }

    private fun givenCheckoutPendingDrinkOrdersFails() {
        given(checkoutService.checkoutPendingDrinkOrders()).willReturn(Completable.error(Error()))
    }

    private fun givenCheckoutServiceReturnsValidDrinks() {
        given(checkoutService.getAllDrinkOrderDetails()).willReturn(Single.just(drinkOrders))
    }

    private fun givenCheckoutServiceReturnsNoDrinks() {
        given(checkoutService.getAllDrinkOrderDetails()).willReturn(Single.just(emptyList()))
    }
}