package co.instil.baristabobshop.drinksmenu.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drink.persistence.Drink
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import co.instil.baristabobshop.prompts.ToastPrompt
import co.instil.baristabobshop.scheduler.BaristaBobSchedulers
import co.instil.baristabobshop.ui.service.ResourceService
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.rx2.asCoroutineDispatcher
import kotlinx.coroutines.rx2.await

class DrinksMenuItemViewModel(
    drink: Drink,
    private val resourceService: ResourceService,
    private val drinksMenuService: DrinksMenuService,
    private val baristaBobSchedulers: BaristaBobSchedulers,
    private val toastPrompt: ToastPrompt
) {

    val name = drink.name
    val cost by lazy { resourceService.getStringResource(R.string.format_currency_gbp).format(drink.cost) }
    val description = drink.description
    val image = drink.imageId
    val id = drink.drinkId

    fun onClickItemBody() = CoroutineScope(Dispatchers.IO).launch(baristaBobSchedulers.uiScheduler().asCoroutineDispatcher()) {
        val toastString = resourceService.getStringResource(R.string.prefix_brewing).format(name)
        toastPrompt.showMessage(toastString)
        drinksMenuService.incrementDrinkOrderItemQuantity(id).await()
    }
}