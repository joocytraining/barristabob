package co.instil.baristabobshop.drinksmenu.periodicmenuupdates

import androidx.work.Worker
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.drinksmenu.service.DrinksMenuService
import org.slf4j.LoggerFactory
import javax.inject.Inject

class DrinksDatabaseUpdateWorker : Worker() {

    private val logger = LoggerFactory.getLogger(DrinksDatabaseUpdateWorker::class.java)

    @Inject lateinit var drinksMenuService: DrinksMenuService

    init {
        Application.dependencies?.inject(this)
    }

    override fun doWork(): Result {
        logger.debug("Running DrinksMenuUpdate job")

        return try {
            drinksMenuService.updateDrinksMenu().blockingAwait()
            Result.SUCCESS
        } catch (error: Throwable) {
            logger.error("DrinksMenuUpdate Job Error", error)
            Result.FAILURE
        }
    }
}