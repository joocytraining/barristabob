package co.instil.baristabobshop

import javax.inject.Inject
import javax.inject.Singleton
@Singleton
open class Settings @Inject constructor() {

    open fun apiUrl() = "http://10.0.2.2:8080/api/"
}