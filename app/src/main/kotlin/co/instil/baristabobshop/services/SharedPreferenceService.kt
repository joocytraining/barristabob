package co.instil.baristabobshop.services

import android.content.Context
import javax.inject.Inject

private const val PREFERENCES_NAME = "BaristaPreferences"

open class SharedPreferenceService @Inject constructor(
    private val context: Context
) {

    fun storeString(key: String, value: String) {
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun retrieveString(key: String): String? {
        val sharedPreferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }
}