package co.instil.baristabobshop.android.ui.dialog

import android.app.Activity
import android.app.AlertDialog
import co.instil.baristabobshop.R
import co.instil.baristabobshop.ui.service.ResourceService

open class DialogFactory(
    val activity: Activity,
    private val resourceService: ResourceService
) {

    open fun confirmationDialog(title: String, message: String) {
        AlertDialog.Builder(activity).setTitle(title)
            .setMessage(message)
            .setNeutralButton(resourceService.getStringResource(R.string.ok_button)) { dialog, _ -> dialog.dismiss() }
            .show()
    }
}