package co.instil.baristabobshop.android.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.android.viewmodel.ActivityViewModel

abstract class BaseActivity<ViewModelType : ActivityViewModel> : AppCompatActivity() {

    var viewModel: ViewModelType? = null
        private set

    lateinit var dependencies: Dependencies

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        dependencies = Dependencies()
    }

    override fun onDestroy() {
        super.onDestroy()

        viewModel?.onDestroy()
    }

    fun attachViewModel(viewModel: ViewModelType) {
        this.viewModel = viewModel
        viewModel.onCreate(this)
    }

    class Dependencies {
        init {
            Application.dependencies?.inject(this)
        }
    }
}