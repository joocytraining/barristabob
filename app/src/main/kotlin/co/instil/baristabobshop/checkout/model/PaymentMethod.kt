package co.instil.baristabobshop.checkout.model

enum class PaymentMethod(val asString: String) {
    CARD("Card"),
    CASH("Cash"),
    NONE("None");
}