/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.recyclerview.viewmodel

import co.instil.baristabobshop.R
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.ui.service.ResourceService

class CheckoutItemViewModel(
    drinkOrderDetails: DrinkOrderDetails,
    resourceService: ResourceService
) {

    private val totalCostString = resourceService
        .getStringResource(R.string.format_currency_gbp)
        .format(drinkOrderDetails.drink.cost * drinkOrderDetails.quantity)

    val name = drinkOrderDetails.drink.name
    val cost = totalCostString
    val quantity = resourceService.getStringResource(R.string.format_quantity).format(drinkOrderDetails.quantity)
}