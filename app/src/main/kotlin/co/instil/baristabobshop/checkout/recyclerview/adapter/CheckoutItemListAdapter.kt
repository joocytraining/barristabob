/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.checkout.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.instil.baristabobshop.checkout.recyclerview.viewmodel.CheckoutItemViewModel
import co.instil.baristabobshop.databinding.CheckoutMenuItemBinding
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.ui.service.ResourceService
import javax.inject.Inject

class CheckoutItemListAdapter @Inject constructor(
    private val resourceService: ResourceService
) : RecyclerView.Adapter<CheckoutItemListAdapter.ViewHolder>() {

    private var checkoutItemsList = listOf<DrinkOrderDetails>()

    override fun getItemCount() = checkoutItemsList.size

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            CheckoutMenuItemBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        )
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.binding.apply {
            vm = CheckoutItemViewModel(checkoutItemsList[position], resourceService)
        }
    }

    fun updateDrinkOrders(drinkOrders: List<DrinkOrderDetails>) {
        checkoutItemsList = drinkOrders
        notifyDataSetChanged()
    }

    inner class ViewHolder(
        val binding: CheckoutMenuItemBinding
    ) : RecyclerView.ViewHolder(binding.root)
}