package co.instil.baristabobshop.security

import co.instil.baristabobshop.security.exceptions.JwtNotInSharedPreferencesException
import co.instil.baristabobshop.services.Base64Service
import co.instil.baristabobshop.services.SharedPreferenceService
import javax.inject.Inject
import javax.inject.Singleton

private const val ENCRYPTED_JWT = "encrypted_jwt"
private const val JWT_INITIALIZATION_VECTOR = "jwt_initialization_vector"

@Singleton
open class JwtService @Inject constructor(
    private val encryptionService: EncryptionService,
    private val sharedPreferenceService: SharedPreferenceService,
    private val base64Service: Base64Service
) {

    fun storeJwt(jwt: String) {
        val encryptedJwtAndInitializationVectorPair = encryptionService.encrypt(jwt)
        val encryptedJwtAsBase64 = base64Service.encodeToString(encryptedJwtAndInitializationVectorPair.encryptedBytes)
        val initializationVectorAsBase64 = base64Service.encodeToString(encryptedJwtAndInitializationVectorPair.initializationVector)
        sharedPreferenceService.storeString(ENCRYPTED_JWT, encryptedJwtAsBase64)
        sharedPreferenceService.storeString(JWT_INITIALIZATION_VECTOR, initializationVectorAsBase64)
    }

    open fun retrieveJwt(): String {
        val encryptedJwt = sharedPreferenceService.retrieveString(ENCRYPTED_JWT) ?: throw JwtNotInSharedPreferencesException()
        val initializationVector = sharedPreferenceService.retrieveString(JWT_INITIALIZATION_VECTOR) ?: throw JwtNotInSharedPreferencesException()

        return encryptionService.decrypt(base64Service.decodeToByteArray(encryptedJwt), base64Service.decodeToByteArray(initializationVector))
    }
}