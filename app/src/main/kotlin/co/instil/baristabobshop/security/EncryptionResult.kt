package co.instil.baristabobshop.security

data class EncryptionResult(val encryptedBytes: ByteArray, val initializationVector: ByteArray)