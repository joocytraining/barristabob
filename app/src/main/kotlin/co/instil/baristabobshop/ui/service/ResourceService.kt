package co.instil.baristabobshop.ui.service

import android.content.Context
import android.support.annotation.StringRes
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
open class ResourceService @Inject constructor(private val applicationContext: Context) {

    open fun getStringResource(@StringRes id: Int) = applicationContext.resources.getString(id)
}