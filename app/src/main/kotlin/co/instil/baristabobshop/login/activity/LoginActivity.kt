package co.instil.baristabobshop.login.activity

import android.databinding.DataBindingUtil
import android.os.Bundle
import co.instil.baristabobshop.Application
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.ui.activity.BaseActivity
import co.instil.baristabobshop.databinding.ActivityLoginBinding
import co.instil.baristabobshop.login.viewmodel.LoginViewModel
import javax.inject.Inject

class LoginActivity : BaseActivity<LoginViewModel>() {

    @Inject lateinit var loginViewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Application.dependencies?.inject(this)
        attachViewModel(loginViewModel)

        DataBindingUtil.setContentView<ActivityLoginBinding>(this, R.layout.activity_login).apply {
            vm = loginViewModel
        }
    }
}
