package co.instil.baristabobshop.login.viewmodel

import android.databinding.ObservableBoolean
import android.util.Log
import co.instil.baristabobshop.R
import co.instil.baristabobshop.android.viewmodel.ActivityViewModel
import co.instil.baristabobshop.databinding.ObservableField
import co.instil.baristabobshop.login.service.LoginService
import co.instil.baristabobshop.navigation.NavigationRouter
import io.reactivex.rxkotlin.subscribeBy
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class LoginViewModel @Inject constructor(
    private val loginService: LoginService,
    private val navigationRouter: NavigationRouter
) : ActivityViewModel() {

    var username = ObservableField("")
    var password = ObservableField("")
    var loginButtonsEnabled = ObservableBoolean(false)

    fun onLoginInformationChanged() {
        loginButtonsEnabled.set(isLoginInformationComplete())
    }

    private fun isLoginInformationComplete() = username.get().isNotEmpty() && password.get().isNotEmpty()

    fun onLoginButtonClicked() {
        loginService.login(username.get(), password.get())
            .subscribeOn(baristaBobSchedulers.ioScheduler())
            .observeOn(baristaBobSchedulers.uiScheduler())
            .subscribeBy(
                onComplete = ::onLoginSuccess,
                onError = ::onLoginError
            )
    }

    private fun onLoginSuccess() {
        navigationRouter.navigateToDrinksOrder()
    }

    private fun onLoginError(error: Throwable) {
        Log.e("Login", error.localizedMessage)
        displayLoginErrorDialog(buildErrorMessage(error))
    }

    private fun buildErrorMessage(error: Throwable) =
        if (isHttp400Error(error)) {
            resourceService.getStringResource(R.string.login_error_dialog_message_http_400)
        } else {
            resourceService.getStringResource(R.string.login_unknown_error_dialog_message)
        }

    private fun isHttp400Error(error: Throwable) =
        error is HttpException && error.code() in 400..499

    private fun displayLoginErrorDialog(errorMessage: String) {
        dialogFactory?.confirmationDialog(resourceService.getStringResource(R.string.login_error_dialog_title), errorMessage)
    }
}