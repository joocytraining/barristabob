package co.instil.baristabobshop.drink.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import co.instil.baristabobshop.converters.Converters

@Database(entities = [Drink::class, DrinkOrder::class], version = 1, exportSchema = false)
@TypeConverters(Converters::class)
abstract class DrinkDatabase : RoomDatabase() {
    abstract fun drinkDatastore(): DrinkDatastore
}