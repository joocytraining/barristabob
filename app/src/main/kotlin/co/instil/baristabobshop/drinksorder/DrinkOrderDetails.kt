/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder

import co.instil.baristabobshop.drink.persistence.Drink

data class DrinkOrderDetails(val quantity: Int, val drinkId: Long, val drink: Drink)