/*
 * Copyright 2018 Instil.
 */

package co.instil.baristabobshop.drinksorder.recyclerview.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import co.instil.baristabobshop.databinding.DrinksOrderItemBinding
import co.instil.baristabobshop.drinksorder.DrinkOrderDetails
import co.instil.baristabobshop.drinksorder.recyclerview.adapter.DrinksOrderListAdapter.ViewHolder
import co.instil.baristabobshop.drinksorder.recyclerview.viewmodel.DrinkOrderDetailsViewModel
import co.instil.baristabobshop.ui.service.ResourceService
import javax.inject.Inject

class DrinksOrderListAdapter @Inject constructor(
    private val resourceService: ResourceService
) : RecyclerView.Adapter<ViewHolder>() {

    var drinkOrders = emptyList<DrinkOrderDetails>()

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            DrinksOrderItemBinding.inflate(LayoutInflater.from(viewGroup.context), viewGroup, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.binding.apply {
            vm = DrinkOrderDetailsViewModel(drinkOrders[position], resourceService)
        }
    }

    override fun getItemCount() = drinkOrders.size

    fun updateDrinkOrders(drinkOrders: List<DrinkOrderDetails>) {
        this.drinkOrders = drinkOrders
        notifyDataSetChanged()
    }

    inner class ViewHolder(
        val binding: DrinksOrderItemBinding
    ) : RecyclerView.ViewHolder(binding.root)
}