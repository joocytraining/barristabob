package co.instil.baristabobshop.event

import io.reactivex.Scheduler

interface BaristaBobSchedulers {

    fun uiScheduler(): Scheduler

    fun ioScheduler(): Scheduler
}