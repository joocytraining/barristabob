package co.instil.baristabobshop.api

import co.instil.baristabobshop.Settings
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RetrofitFactory @Inject constructor(
    val settings: Settings
) {

    inline fun <reified T> createInstance(): T =
        Retrofit.Builder()
            .baseUrl(settings.apiUrl())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create(T::class.java)
}