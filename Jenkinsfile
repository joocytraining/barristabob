properties([
    buildDiscarder(logRotator(numToKeepStr: "25"))
])

node("android") {
    stage("Checkout") {
        checkout scm
    }

    stage("Install Android Dependencies") {
        installAndroidSdkPackages()
    }

    stage("Build/Analyse/Unit Test") {
        build()
    }

    stage("Integration Test") {
        executeIntegrationTests()
    }
}

def build() {
    try {
        sh "./gradlew clean build --no-build-cache"
        junit testResults: "**/testReleaseUnitTest/TEST-*.xml"
        openTasks pattern: "**/src/main/**/*.java, **/src/main/**/*.kt"
    } catch(e) {
        slackNotifyError("Build failed")
        error "Build failed"
    }
}

def executeIntegrationTests() {
    try {
        withAvd(hardwareProfile: "Nexus 9", systemImage: "system-images;android-26;google_apis;x86") {
            sh "./gradlew connectedAndroidTest"
            junit testResults: "**/androidTest-results/connected/TEST-*.xml"
        }
    } catch(e) {
        slackNotifyError("Integration tests failed")
        error "Integration tests failed"
    }
}

def slackNotify(message) {
    slackSend channel: "#intern-training-2018", color: "good", message: message
}

def slackNotifyError(message) {
    slackSend channel: "#intern-training-2018", color: "danger", message: "${message}, see ${env.BUILD_URL}console"
}
